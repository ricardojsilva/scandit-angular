import { Component, OnInit } from '@angular/core';
import {
  Barcode,
  BarcodePicker,
  Camera,
  CameraAccess,
  CameraSettings,
  ScanResult,
  ScanSettings
} from 'scandit-sdk';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public activeSettings: ScanSettings;
  public active: string;
  public settings128: ScanSettings;
  public settings39: ScanSettings;
  public settingsEan13: ScanSettings;
  public settingsQR: ScanSettings;
  public settingsall: ScanSettings;
  public scannerGuiStyle: BarcodePicker.GuiStyle =
    BarcodePicker.GuiStyle.VIEWFINDER;
  public activeCamera: Camera;
  public cameraSettings: CameraSettings;
  public scanningPaused = false;
  public visible = true;
  public fps = 30;
  public videoFitContain: BarcodePicker.ObjectFit =
    BarcodePicker.ObjectFit.CONTAIN;
  public videoFitCover: BarcodePicker.ObjectFit = BarcodePicker.ObjectFit.COVER;
  public videoFit: BarcodePicker.ObjectFit = this.videoFitCover;
  public scannedCodes: Barcode[] = [];
  public isReady = false;
  public enableCameraSwitcher = true;
  public enablePinchToZoom = true;
  public enableTapToFocus = true;
  public enableTorchToggle = true;
  public enableVibrateOnScan = true;
  public cameraAccess = true;
  public enableSoundOnScan = true;
  public possibleCameras: Camera[] = [];

  constructor(public scanService: AppService) {
    this.settings128 = new ScanSettings({
      enabledSymbologies: [Barcode.Symbology.CODE128],
      codeDuplicateFilter: 1000
    });
    this.settings39 = new ScanSettings({
      enabledSymbologies: [Barcode.Symbology.CODE39],
      codeDuplicateFilter: 1000
    });
    this.settingsEan13 = new ScanSettings({
      enabledSymbologies: [Barcode.Symbology.EAN13],
      codeDuplicateFilter: 1000
    });
    this.settingsQR = new ScanSettings({
      enabledSymbologies: [Barcode.Symbology.QR],
      codeDuplicateFilter: 1000
    });
    this.settingsall = new ScanSettings({
      enabledSymbologies: [
        Barcode.Symbology.EAN8,
        Barcode.Symbology.EAN13,
        Barcode.Symbology.UPCA,
        Barcode.Symbology.UPCE,
        Barcode.Symbology.CODE128,
        Barcode.Symbology.CODE39,
        Barcode.Symbology.CODE93,
        Barcode.Symbology.INTERLEAVED_2_OF_5,
        Barcode.Symbology.QR
      ]
    });
    this.activeSettings = this.settingsall;

    CameraAccess.getCameras().then(cameras => {
      this.possibleCameras = cameras;
    });

    this.cameraSettings = {
      resolutionPreference: CameraSettings.ResolutionPreference.FULL_HD
    };
  }
  ngOnInit() {
    this.scanService.loadData();
  }

  public toggleGuiStyle(): void {
    if (this.scannerGuiStyle === BarcodePicker.GuiStyle.VIEWFINDER) {
      this.scannerGuiStyle = BarcodePicker.GuiStyle.LASER;
    } else {
      this.scannerGuiStyle = BarcodePicker.GuiStyle.VIEWFINDER;
    }
  }

  public onScan(result: ScanResult): void {
    this.scannedCodes = this.scannedCodes.concat(result.barcodes);
  }
}
