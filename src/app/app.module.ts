import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ScanditSdkModule } from 'scandit-sdk-angular';
import { AppComponent } from './app.component';

const licenseKey =
  // tslint:disable-next-line: max-line-length
  'Afe9LlhwRpKYPHgW9zFaSSwGXJXGAYbWMGqSoI1XPk1XdRtRbXgzPaRzxgoIcS/E6itr0hthF2cAbJ5m2mpwWII4apknavBWZkbfCsl6w+HvGw+xDD68rKExYYS8RY0eCV6vOidu0QhClPtQL9G7wjmefdcwNmImQHiOyW1982AO0mELNH8b6vIKlTvaPROSyG1GHB5/Hiw5/qhqAPHOSJmLDP1Ugp7qp0XdLFTZr8pKqDttGU19rbhkXoCMnsYtQ/azTNYOVg6Fu7sfqjWo06vow95JhdmLuJ9BL3jpSyWg8eve51ZRl37ykMI95lABlmHqm4iI77lJ8zOy1AHRdjfMnGIx0AeqovHM7+GE6uK5td9xJijE7wif1Jl+4Bg8eLc2LI8ss2S5XDBGsPcaFvfU5mkkH28TKzXDdu6EzTNSTFdEDAB6VLugQGAXtzF0nHfeDRxYzMYrAzB4y8uCwTX+h+NxuxZwP8+3BG9W9p9XYl37dOXtKnqCCW8OqEnY4okpXx1BM941QTyPhcL7exfXZSHbuvYnLu86U7SmdW37wibOcpqfky81ogcm0k0mkHqYotFJ5cnYn3IkxY4lk4VG3pSxgmFxV0++jbUbVcoxI6KMwg+NNQv54Xj84gUsikaThgPlWvx0bOVVn2L8BEzanXj8VWlv15Ih/5S+XFNoWQb9fe7OGPXCa4ZJxkKYq3pn09mI/G7gm0GDlsQjTM11e4OT3YgyxbzjTLrpKvJAqdi48zoq0w6/nCuhnOsP3LNP0oplY+yvCPCdB6SbAldEIYT+hrjndGFO';

const engineLocation = '../assets/';

@NgModule({
  declarations: [AppComponent],
  imports: [
    CommonModule,
    BrowserModule,
    FlexLayoutModule,
    HttpClientModule,
    NgScrollbarModule,
    ScanditSdkModule.forRoot(licenseKey, engineLocation)
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
